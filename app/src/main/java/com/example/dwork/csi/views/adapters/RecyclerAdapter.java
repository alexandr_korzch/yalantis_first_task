package com.example.dwork.csi.views.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.dwork.csi.MyApplication;
import com.example.dwork.csi.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import rx.android.observables.ViewObservable;
import rx.functions.Action1;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<String> urlList;
    private Action1<View> clickAction;
    private int imageWidth;

    public RecyclerAdapter(Context context, Action1<View> clickAction) {
        this.clickAction = clickAction;
        imageWidth = getImageWidth(context);
    }

    private int getImageWidth(Context context) {
        float image_margin = context.getResources().getDimension(R.dimen.image_margin);
        float horizontal_margin = context.getResources().getDimension(R.dimen.fragment_horizontal_padding);
        int imageWidth = (int) ((MyApplication.displayWidth - image_margin - horizontal_margin) / 2);
        return imageWidth;
    }

    @Override
    public int getItemCount() {
        if (urlList != null) {
            return urlList.size();
        } else {
            return 0;
        }
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        LinearLayout.LayoutParams params
                = new LinearLayout.LayoutParams(imageWidth, LinearLayout.LayoutParams.MATCH_PARENT);

        viewHolder.image_container.setLayoutParams(params);

        Uri uri = Uri.parse(urlList.get(position));
        viewHolder.imgViewIcon.setImageURI(uri);

        ViewObservable.clicks(viewHolder.imgViewIcon, false).subscribe(clickAction);
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public SimpleDraweeView imgViewIcon;
        public LinearLayout image_container;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            image_container = (LinearLayout) itemLayoutView.findViewById(R.id.image_container);
            imgViewIcon = (SimpleDraweeView) itemLayoutView.findViewById(R.id.my_image_view);
        }
    }
}